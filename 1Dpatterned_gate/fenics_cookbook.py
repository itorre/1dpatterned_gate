#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''Useful classes and functions to work with FENICS'''
__version__='1.1'
__author__='Iacopo Torre'
################## Import modules #############################################
import numpy as np
import matplotlib.pyplot as plt
import dolfin as dol
import figsizes as fs
################## Class for matrix coefficients ##############################
class Matrix_Expression(dol.UserExpression):
    '''class for matrix expression, default dimension is 2X2. 
    Can be initializex with either mesh functions or numbers
    Parameters for initialization:
    chack the markerfunction initialization
    '''
    def __init__(self,
                 entries,
                 marker = None,
                 dimension = 2,
                 scalar = False,
                 *args,
                 **kwargs):
        self.entries = entries #entries of the matrix as a flattened list (C-ordering)
        self.marker = marker
        self.dimension = int(dimension) #dimension of the matrix, default is 2X2
        self.scalar = scalar #whether the matrix is a scalar matrix. Default False.
        #check the type of initialization (numbers or meshfunctions)        
        self.is_constant = all(isinstance(element,(int,float,complex)) 
            for element in self.entries) and marker is None#true if all the entries are numbers
        self.is_meshfunction = all(
            isinstance(element,(dol.cpp.mesh.MeshFunctionDouble,
            dol.cpp.mesh.MeshFunctionInt,dol.cpp.mesh.MeshFunctionSizet))
             for element in self.entries) #true if all the entries are meshfunctions
        self.is_markerfunction = not (marker is None) #add checks
        dol.UserExpression.__init__(self, *args, **kwargs) #execute the __init__ of the superclass
    def eval_cell(self, value, x, cell):
        if self.is_constant:
            if self.scalar:
                value[:] = self.entries * np.ravel(np.identity(self.dimension))
            else:
                value[:] = self.entries
        elif self.is_meshfunction:
            if self.scalar:
                value[:] = (self.entries[0][cell.index] 
                            * np.ravel(np.identity(self.dimension)))
            else:
                value[:] = [function[cell.index] for function in self.entries]
        elif self.is_markerfunction:
            if self.scalar:
                value[:] = (self.entries[self.marker[cell.index]] 
                            * np.ravel(np.identity(self.dimension)))
            else:
                value[:] = self.entries[self.marker[cell.index]]
        else:
            raise TypeError('''Expected either all numbers or all 
                             MeshFunction for matrix entries''')
    def value_shape(self):
        if self.dimension >=2:
                return (self.dimension,self.dimension)
        else:
            return ()
#################### Functions for boundaries and subdomains ##################
def close_to_segment(x, r0, r1, tol):
    '''Function that returns whether a point x is closer than tol to a segment 
    identified by its endpoints r0 and r1.
    Parameters : 
    x :
    r0 :
    r1 :
    tol :
    Returns :'''
    l = np.sqrt((r0[0]-r1[0])**2+(r0[1]-r1[1])**2)
    m = [(r1[0] + r0[0])/2. , (r1[1] + r0[1])/2.]
    if l < tol:
        return (x[0]-m[0])**2 + (x[1]-m[1])**2 <= tol**2
    else:
        f = ((r1[1]-r0[1])*(x[0]-r0[0])-(r1[0]-r0[0])*(x[1]-r0[1]))/l
        g = ((r1[0]-r0[0]) * (x[0] - m[0]) + (r1[1]-r0[1]) * (x[1]-m[1]))/l
        return ((abs(f) <= tol) and (abs(g) <= 0.5 * l + tol))
    #return (abs((r1[1]-r0[1])*(x[0]-r0[0])-(r1[0]-r0[0])*(x[1]-r0[1]))<=
    #    tol*np.sqrt(((r0[0]-r1[0])**2+(r0[1]-r1[1])**2)) 
    #    and abs((r1[0]-r0[0])*(x[0]-(r0[0]+r1[0])/2.)+(r1[1]-r0[1])*(x[1]-(r0[1]+r1[1])/2.))
    #    <=((r0[0]-r1[0])**2+(r0[1]-r1[1])**2)/2.+tol*np.sqrt(((r0[0]-r1[0])**2+(r0[1]-r1[1])**2)))  

def close_to_line(x,line,tol):
    '''returns whether a point is closer than tol to a line. line is a list of 
    points in 2D space like [[0.,0.],[0.,1.],[1.,1.]]'''
    result = False
    for i in range(len(line)-1):
        result = result or close_to_segment(x,line[i],line[i+1],tol)
    return result

class Boundary_Close_To_Line(dol.SubDomain):
    '''class for boundary points close to a line'''
    def __init__(self, line, tol = dol.DOLFIN_EPS):
        dol.SubDomain.__init__(self)
        self.line = line
        self.tol = tol
    def inside(self, x, on_boundary):
        return on_boundary and close_to_line(x, self.line, self.tol)

class Close_To_Line(dol.SubDomain):
    '''class for points close to a line'''
    def __init__(self, line, tol = dol.DOLFIN_EPS):
        dol.SubDomain.__init__(self)
        self.line = line
        self.tol = tol
    def inside(self, x, on_boundary):
        return close_to_line(x,self.line, self.tol)

class Periodic_Boundary(dol.SubDomain):
    def __init__(self, direction = 0, xmin = 0, xmax = 1):
        self.direction = direction
        self.xmin = xmin
        self.xmax = xmax
        dol.SubDomain.__init__(self)
    def inside(self, x, on_boundary):
        return bool(x[self.direction] < self.xmin + dol.DOLFIN_EPS 
                    and x[self.direction] > self.xmin - dol.DOLFIN_EPS 
                    and on_boundary)
    def map(self, x, y):
        for i in range(len(x)):
            if i == self.direction:
                y[i] = x[i] - self.xmax + self.xmin
            else:
                y[i] = x[i]
############ Create a cell function from callable #############################
def create_cellfunction(sample_mesh, f):
    '''creates a cell function from a mesh and a python function'''
    fun = dol.MeshFunction("double", sample_mesh, sample_mesh.topology().dim())
    for cell in dol.cells(sample_mesh):
        fun[cell] = f(cell.midpoint().x(),cell.midpoint().y())
    return fun
########### Plotting functions#################################################
def plot_function(coordinates, cells, function,
                  total_width, total_height, label, filename,
                  field = None, cells_centers = None,
                  facet_function = None, mesh = None, facet_markers = None,
                  code_string = None, **kwargs):
    '''utility function for plotting'''
    plt.figure(figsize = fs.figsize2d(total_width / total_height))
    plt.clf()
    ax = plt.axes(fs.axes2d(total_width / total_height))
    ax.triplot(coordinates[:,0], coordinates[:,1], triangles = cells, 
                c = 'k', lw = 0.1)

    if not field is None:
        plt.quiver(cells_centers[:,0],cells_centers[:,1], field[:,0], field[:,1] )
    if not facet_function is None:
        plot_facet_function(facet_function, mesh, mesh_edges = False, 
            markers = facet_markers)
    if not function is None:
        p1 = ax.tripcolor(coordinates[:,0], coordinates[:,1], cells, 
                        function, **kwargs)
        cb_axes = plt.axes(fs.axescb(total_width / total_height))
        cb = plt.colorbar(mappable = p1, cax = cb_axes)
        cb.set_label(label)
    ax.set_xlim(0, total_width)
    ax.set_ylim(0, total_height)
    ax.set_xlabel(r'$x~[{\rm \mu m}]$')
    ax.set_ylabel(r'$y~[{\rm \mu m}]$')
    
    if not code_string is None:
        exec(code_string)
    plt.savefig(filename)

def plot_facet_function(facet_func, mesh, mesh_edges = True, markers = None):
    x_list, y_list = [],[]
    if markers is None:
        for facet in dol.facets(mesh):
            mp = facet.midpoint()
            x_list.append(mp.x())
            y_list.append(mp.y())
        new_facet_func = facet_func.array()
    else:
        i = 0
        new_facet_func = []
        for facet in dol.facets(mesh):
            if facet_func[i] in markers:
                mp = facet.midpoint()
                x_list.append(mp.x())
                y_list.append(mp.y())
                new_facet_func.append(facet_func[i])
            i+=1
    if mesh_edges:
        plt.triplot(mesh.coordinates()[:,0], mesh.coordinates()[:,1], 
            triangles = mesh.cells(), c = 'k', lw = 0.1)
    plt.scatter(x_list, y_list, s=.5, c=new_facet_func,
                          linewidths=1, zorder = 10, cmap = 'Reds')