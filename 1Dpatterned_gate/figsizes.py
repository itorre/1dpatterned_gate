StandardWidth=246.0/72.27 #one PRB column

def figsize1d(aspect_ratio=1.618,journal='PRB'):
    '''Returns figuresize for a 1-d plot given the as_integer_ratio (x/y) of the axis.
    Usage:
    import matplotlib.pyplot as plt
    from figsize import *
    plt.figure(figsize=figsize1d())'''
    if journal != 'PRB':
        raise NotImplementedError('Only PRB style implemented')
    return [StandardWidth,StandardWidth*(0.78/aspect_ratio+0.22/1.618)]
def axes1d(aspect_ratio=1.618,journal='PRB'):
    '''Returns axisdimensions for a 1-d plot given the as_integer_ratio (x/y) of the axis.
    Usage:
    import matplotlib.pyplot as plt
    from figsize import *
    plt.axes(axes1d())'''
    if journal != 'PRB':
        raise NotImplementedError('Only PRB style implemented')
    return [0.18,0.18/(1.618*(0.78/aspect_ratio+0.22/1.618)),0.78,0.78/(aspect_ratio*(0.78/aspect_ratio+0.22/1.618))]
def figsize2d(aspect_ratio=1.618,journal='PRB'):
    '''Same as figsize1d() for 2D plots'''
    if journal != 'PRB':
        raise NotImplementedError('Only PRB style implemented')
    return [StandardWidth,StandardWidth*(0.6/aspect_ratio+0.22/1.618)]
def axes2d(aspect_ratio=1.618,journal='PRB'):
    '''Same as axes1d() for 2D plots'''
    if journal != 'PRB':
        raise NotImplementedError('Only PRB style implemented')
    return [0.18,0.18/(1.618*(0.6/aspect_ratio+0.22/1.618)),0.6,0.6/(aspect_ratio*(0.6/aspect_ratio+0.22/1.618))]
def axescb(aspect_ratio=1.618,journal='PRB'):
    '''Axes for the colorbar in a 2D plot
    Usage:
    plt.axes(axescb(1./np.sqrt(3)))
    '''
    if journal != 'PRB':
        raise NotImplementedError('Only PRB style implemented')
    return [0.8,0.18/(1.618*(0.6/aspect_ratio+0.22/1.618)),0.02,0.6/(aspect_ratio*(0.6/aspect_ratio+0.22/1.618))]