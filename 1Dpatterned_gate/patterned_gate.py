#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''electrostatic solver for 1D periodic structures including quantum 
capacitances'''
__version__ = '1.0'
__author__ = 'Iacopo Torre'
#################################### Import modules ###########################
import os
import mshr
from fenics_cookbook import *
from materials import *
###############################################################################
def simulation(total_width,
               layers, 
               holes,
               sheets,
               gate_voltages,
               output_folder,
               resolution = 10,
               refinement_region = []):
    '''runs a simulation and saves output'''
    #################################### Make output directory ################
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    ################################### Build geometry ########################
    geometry = build_geometry(total_width,
                              layers, 
                              holes,
                              sheets,
                              resolution = resolution,
                              refinement_region = refinement_region)
    #################### Print gate voltages and otput directory ##############
    print(29 * '#' + 'Gate voltages' + 30 * '#')
    for gate in gate_voltages:
        print(gate, gate_voltages[gate])
    print(72*'#')
    print('Output directory = {}'.format(output_folder))
    print(72*'#')
    #####################################Save mesh data########################
    np.savetxt(output_folder + '/vertices.txt', geometry['coordinates'])
    np.savetxt(output_folder + '/triangles.txt', geometry['cells'])
    np.savetxt(output_folder + '/cell_centers.txt', geometry['cells_centers'])
    np.savetxt(output_folder + '/subdomains.txt',
               geometry['subdomain_marker'].array())
    ##################################### Compute solution ####################
    phi = solve_FEM(geometry = geometry, gate_voltages = gate_voltages)
    #####################################Save solution#########################
    phi_vertices = phi.compute_vertex_values(geometry['mesh'])
    np.savetxt(output_folder + '/potential_vertices.txt', phi_vertices)
    ##################################### Calculate fields and save ###########
    fields, sheet_values = calculate_fields(phi = phi, geometry = geometry)
    np.savetxt(output_folder + '/E_cells.txt', fields['E_cells'])
    np.savetxt(output_folder + '/D_cells.txt', fields['D_cells'])
    np.savetxt(output_folder + '/E_vertices.txt', fields['E_vertices'])
    np.savetxt(output_folder + '/D_vertices.txt', fields['D_vertices'])
    np.savetxt(output_folder+'/epsilon_cells.txt',
               geometry['epsilon_cells'].reshape(-1,4))
    np.savetxt(output_folder + '/x.txt', sheet_values['x_sampling'])
    for i, sheet in enumerate(sheets):
        np.savetxt(output_folder + '/rho{}.txt'.format(sheet['index']),
                   sheet_values['charge_densities_sheets'][i,:])
        np.savetxt(output_folder + '/potential{}.txt'.format(sheet['index']),
                   sheet_values['potential_sheets'][i,:])
    print('Simulation ended successfully.')
    return {'geometry' : geometry,
            'potential' : {'phi' : phi, 'potential_vertices' : phi_vertices},
            'fields' : fields,
            'sheet_values' : sheet_values}
###############################################################################
def build_geometry(total_width,
                   layers, 
                   holes,
                   sheets,
                   resolution = 10,
                   refinement_region = []):
    '''Function that builds the geometrical elements'''
    #add some check
    total_height = np.sum([layer['thickness'] for layer in layers])
    n_layers = len(layers)
    n_holes = len(holes)
    n_sheets = len(sheets)
    z = 0.
    for layer in layers:
        layer['z1'] = z
        z += layer['thickness']
        layer['z2'] = z
    del(z)
    for hole in holes:
        for layer in layers:
            if layer['index'] == hole['layer']:
                hole['z1'] = layer['z1']
                hole['z2'] = layer['z2']
    for sheet in sheets:
        for layer in layers:
            if layer['index'] == sheet['interface'][0]:
                sheet['z'] = layer['z2']
        for layer in layers:
            if layer['index'] == sheet['interface'][1]:
                sheet['z'] = layer['z1']
    ####################### print parameters ##################################
    if True:
        print(32*'#' + 'Geometry' + 32*'#')
        print('Period =', total_width)
        for layer in layers:
            print('{} : Layer z1 = {}, z2 = {}, thickness = {}, material = {}'
                .format(layer['index'], layer['z1'], layer['z2'],
                        layer['thickness'], layer['material']))
        for hole in holes:
            print('''{} : Hole in layer {} , center = {} , width = {}, 
                  z1 = {},  z2 = {}, material = {}'''.format(
                        hole['index'],
                        hole['layer'], 
                        0.5 * (hole['x1']+hole['x2']), 
                        abs(hole['x1']-hole['x2']),
                        hole['z1'],
                        hole['z2'],
                        hole['material']))
        for sheet in sheets:
            print('{} : Sheet z = {}, material = {}'.format(
                sheet['index'], 
                sheet['z'],
                sheet['material']))
    #################################### domain definition ####################
    domain = mshr.Rectangle(dol.Point(0., 0.), 
                            dol.Point(total_width, total_height))
    for hole in holes:
        subdomain = mshr.Rectangle(
            dol.Point(hole['x1'], hole['z1']), 
            dol.Point(hole['x2'], hole['z2']))
        if hole['material'] == 'conductor':
            domain -= subdomain
        else:
            domain.set_subdomain(hole['index'], subdomain)
    ######################### set subdomains ##################################
    for layer in layers:
        subdomain = mshr.Rectangle(dol.Point(0., layer['z1']),
                                   dol.Point(total_width, layer['z2']))
        for hole in holes:
            if hole['layer'] == layer['index']:
                subdomain -= mshr.Rectangle(
                    dol.Point(hole['x1'], hole['z1']), 
                    dol.Point(hole['x2'], hole['z2']))
        domain.set_subdomain(layer['index'], subdomain)
    #################################### Mesh generation ######################
    my_mesh = mshr.generate_mesh(domain, resolution)
    subdomain_marker = dol.MeshFunction('size_t',
                                        my_mesh, 
                                        2, 
                                        my_mesh.domains())
    #################################### Mesh refinements #####################
    my_mesh, subdomain_marker = refine_mesh(my_mesh, 
                                            subdomain_marker, 
                                            refinement_region)
    #################################### Volume measure ####################### 
    dx = dol.Measure('dx', 
                     domain = my_mesh, 
                     subdomain_data = subdomain_marker)
    ################### Boundary definition and boundary measures #############
    boundary_marker = dol.MeshFunction('size_t', my_mesh, 1)
    internal_boundary_marker = dol.MeshFunction('size_t', my_mesh, 1)
    gates = []
    for sheet in sheets:
        if sheet['z'] == 0. or sheet['z'] == total_height:
            contour = Boundary_Close_To_Line([[0., sheet['z']], 
                                              [total_width, sheet['z']]])
            contour.mark(boundary_marker, sheet['index'])
        else: 
            contour = Close_To_Line([[0., sheet['z']], 
                                     [total_width, sheet['z']]])
            contour.mark(internal_boundary_marker, sheet['index'])
        if sheet['material'] == 'conductor':
            gates.append({'index' : sheet['index'], 'contour' : contour})
    for hole in holes:
        if hole['material'] == 'conductor':
            contour = Boundary_Close_To_Line([[hole['x1'], hole['z1']],
                                              [hole['x2'], hole['z1']],
                                              [hole['x2'], hole['z2']],
                                              [hole['x1'], hole['z2']],
                                              [hole['x1'], hole['z1']]])
            contour.mark(boundary_marker, hole['index'])
            gates.append({'index' : hole['index'], 'contour' : contour})
    ds = dol.Measure('ds', 
                     domain = my_mesh, 
                     subdomain_data = boundary_marker)
    dS = dol.Measure('dS', 
                     domain = my_mesh, 
                     subdomain_data = internal_boundary_marker)
    #################################### Define Dielectric constant ###########
    max_index = max([element['index'] for element in layers + holes])
    eps = np.zeros([max_index + 1, 4])
    for element in layers + holes:
        if element['material'] != 'conductor':
            eps[element['index'],:] = np.ravel(
                dielectric_constants[element['material']])
    epsilon = Matrix_Expression(eps, marker = subdomain_marker)
    W2 = dol.TensorFunctionSpace(my_mesh,
                                 "DP", 0)
    epsilon_projected =dol.project(epsilon, W2)
    epsilon_cells = np.array([epsilon_projected.vector().get_local()
        [W2.dofmap().cell_dofs(cell.index())] 
        for cell in dol.cells(my_mesh)]).reshape(-1,2,2)

    return {'mesh' : my_mesh,
            'coordinates' : my_mesh.coordinates(),
            'cells' : my_mesh.cells(),
            'cells_centers' : [(cell.midpoint().x(), cell.midpoint().y()) 
                                for cell in dol.cells(my_mesh)],
            'subdomain_marker' : subdomain_marker,
            'boundary_marker' : boundary_marker,
            'internal_boundary_marker' : internal_boundary_marker,
            'volume_measure' : dx,
            'boundary_measure' : ds,
            'internal_boundary_measure' : dS,
            'epsilon' : epsilon,
            'epsilon_cells' : epsilon_cells,
            'gates' : gates,
            'total_height' : total_height,
            'total_width' : total_width,
            'layers' : layers,
            'holes' : holes,
            'sheets' : sheets}
###############################################################################
def refine_mesh(my_mesh, subdomain_marker, refinement_region):
    for ref in refinement_region:
        ref_marker = dol.MeshFunction('bool', my_mesh, 2, False)
        for cell in dol.cells(my_mesh):
            if (cell.midpoint().y()> ref[0] - dol.DOLFIN_EPS
                and cell.midpoint().y()< ref[1] + dol.DOLFIN_EPS):
                ref_marker[cell] = True
        my_mesh = dol.refine(my_mesh, ref_marker, redistribute = False)
        subdomain_marker = dol.adapt(subdomain_marker, my_mesh)
    return my_mesh, subdomain_marker
###############################################################################
def solve_FEM(geometry, gate_voltages):
    #################################### Define periodic boundary #############
    periodic_boundary = Periodic_Boundary(0, 0., geometry['total_width'])
    ############################## Function space and Dirichlet BCs ###########
    V = dol.FunctionSpace(geometry['mesh'], 
                      "P", 1,  #linear Lagrange elements
                      constrained_domain = periodic_boundary)
    bc0 = [dol.DirichletBC(V, 
                           dol.Constant(gate_voltages[gate['index']]), 
                           gate['contour']) for gate in geometry['gates']]
    v = dol.TestFunction(V)
    source_terms = [dol.Constant(0.) * v * geometry['boundary_measure']]
    for sheet in geometry['sheets']:
        if sheet['material'] == 'fixed_density':
            if (np.isnan(sheet['interface'][0]) 
                or np.isnan(sheet['interface'][1])):
                source_terms.append(
                    v * sheet['expression'] / EPSILON_0
                     * geometry['boundary_measure'](sheet['index']))
            else:
                source_terms.append(
                    dol.avg(v) * sheet['expression'] / EPSILON_0
                    * geometry['internal_boundary_measure'](sheet['index']))
    L = sum(source_terms)
    ################################### Check if the problem is linear ########
    linear = all([(sheet['material'] == 'conductor' 
                or sheet['material'] == 'fixed_density') 
                for sheet in geometry['sheets']])
    if linear:
        u = dol.TrialFunction(V)
        a = (dol.inner(geometry['epsilon'] * dol.grad(u), dol.grad(v))
            * geometry['volume_measure'])
        u = dol.Function(V)
        problem = a==L
        dol.solve(problem, u, bc0)
        return u
    else: 
        u = dol.Function(V)
        du = dol.TrialFunction(V)
        a = (dol.inner(geometry['epsilon'] * dol.grad(u), dol.grad(v))
            * geometry['volume_measure'])

        non_linear_terms = []
        non_linear_terms_der = []
        for sheet in geometry['sheets']:
            if (sheet['material'] != 'conductor' 
                and sheet['material'] != 'fixed_density'):
                rho_funct = rho_dict[sheet['material']]['funct'](
                    u, gate_voltages[sheet['index']], **sheet['parameters'])
                rho_der1 = rho_dict[sheet['material']]['der1'](
                    u, gate_voltages[sheet['index']], **sheet['parameters'])
                non_linear_terms.append(dol.avg(
                    v
                    * rho_funct / EPSILON_0)
                    * geometry['internal_boundary_measure'](sheet['index']))
                non_linear_terms_der.append(dol.avg(
                    v 
                    * du
                    * rho_der1 / EPSILON_0)
                    * geometry['internal_boundary_measure'](sheet['index']))
        N = sum(non_linear_terms)
        dN = sum(non_linear_terms_der)
        da = (dol.inner(geometry['epsilon'] * dol.grad(du), dol.grad(v))
            * geometry['volume_measure'])
        F = a - L - N
        J = da - dN
        #problem = F==0
        #solve(problem, u, bc0)
        problem = dol.NonlinearVariationalProblem(F, u, bc0, J)
        solver = dol.NonlinearVariationalSolver(problem)
        solver.solve()
        return u
###############################################################################
def calculate_fields(phi, geometry):
    periodic_boundary = Periodic_Boundary(0,0.,geometry['total_width'])
    W = dol.VectorFunctionSpace(geometry['mesh'],
                                "DP", 0, 
                                constrained_domain = periodic_boundary)
    E = dol.project(-dol.grad(phi), W)
    D = dol.project(-EPSILON_0 * geometry['epsilon'] * dol.grad(phi), W) 
    E_x, E_y = E.split(deepcopy = False)
    D_x, D_y = D.split(deepcopy = False)
    E_x_vertices = E_x.compute_vertex_values(geometry['mesh'])
    E_y_vertices = E_y.compute_vertex_values(geometry['mesh'])
    E_vertices = np.stack((E_x_vertices, E_y_vertices), axis = -1)
    D_x_vertices = D_x.compute_vertex_values(geometry['mesh'])
    D_y_vertices = D_y.compute_vertex_values(geometry['mesh'])
    D_vertices = np.stack((D_x_vertices, D_y_vertices), axis = -1)
    E_cells = np.array(
        [E.vector().get_local()[W.dofmap().cell_dofs(cell.index())] 
               for cell in dol.cells(geometry['mesh'])])
    D_cells = np.array(
        [D.vector().get_local()[W.dofmap().cell_dofs(cell.index())] 
               for cell in dol.cells(geometry['mesh'])])
    
    
    x = np.linspace(0 + dol.DOLFIN_EPS, 
                    geometry['total_width'] - dol.DOLFIN_EPS, 
                    num = 200)
    rho = np.zeros([len(geometry['sheets']), len(x)])
    potential_sheets = np.zeros([len(geometry['sheets']), len(x)])
    for i, sheet in enumerate(geometry['sheets']):
        if np.isnan(sheet['interface'][0]):
            D_y_minus_vals = np.zeros_like(x)
        else:
            D_y_minus_vals = np.array([D_y(xi, sheet['z'] - dol.DOLFIN_EPS) 
                                       for xi in x])
        if np.isnan(sheet['interface'][1]):
            D_y_plus_vals = np.zeros_like(x)
        else:
            D_y_plus_vals = np.array([D_y(xi, sheet['z'] + dol.DOLFIN_EPS) 
                                      for xi in x])
        rho_vals = (D_y_plus_vals - D_y_minus_vals)
        rho[i,:] = rho_vals
        potential_sheets[i,:] = [phi(xi, sheet['z']) for xi in x]

    return ({'E' : E,
             'D' : D,
             'E_cells' : E_cells,
             'D_cells' : D_cells,
             'E_vertices' : E_vertices,
             'D_vertices' : D_vertices},
            {'x_sampling' : x,
             'charge_densities_sheets' : rho,
             'potential_sheets' : potential_sheets})
########### Examples ##########################################################
#layers from bottom to top, don't use index : 0, do not repeat index
#units: lenghts in um, potential in volts, charge in Coulombs, capacties in 
# Farad
#input geometric dimensions in um, voltages in Volts, charge densities in 
# Coulomb/um^d
#output potential in volt, E in V/um D in C/um^2, charge densities in C/um^d
###############################################################################
def _test_linear():
    simulation_input = {
        'total_width' : .080,
        'layers' : [
            {'index' : 1, 'thickness' : 0.2850, 'material' : 'SiO2'},
            {'index' : 2, 'thickness' : 0.0130, 'material' : 'vacuum'},
            {'index' : 3, 'thickness' : 0.0035, 'material' : 'hBN'}, 
            {'index' : 4, 'thickness' : 0.0060, 'material' : 'hBN'}, 
            {'index' : 5, 'thickness' : 0.1000, 'material' : 'vacuum'}
            ],
        'holes' :  [
            {'index' : 6, 'layer' : 2, 'x1' : .020, 'x2' : .060, 
            'material' : 'conductor'}
            ], 
        'sheets' : [
            {'index' : 7, 'interface' : (np.nan, 1), 'material' : 'conductor'},
            {'index' : 8, 'interface' : (3,4), 'material' : 'fixed_density', 
            'expression' : dol.Expression(
                '4.e-15*pi*cos(2.*pi*{}*x[0]/{})'.format(3,0.08), degree = 2)}
            ],
        'gate_voltages' : {6 : 1., 7 : 0., 8 : 0.},  
        'output_folder' : 'testing_linear',
        'resolution' : 10,
        'refinement_region' : [(0.285, 0.3075)]
        }
    simulation_output = simulation(**simulation_input)
    #####################domain plot###########################################
    plot_function(
        coordinates = simulation_output['geometry']['coordinates'],
        cells = simulation_output['geometry']['cells'],
        function = simulation_output['geometry']['subdomain_marker'].array(),
        total_width = simulation_output['geometry']['total_width'],
        total_height = simulation_output['geometry']['total_height'],
        label = r'Domain index',
        filename = simulation_input['output_folder'] + '/domains.pdf')
    #####################potential plot########################################
    plot_function(
        coordinates = simulation_output['geometry']['coordinates'],
        cells = simulation_output['geometry']['cells'],
        function = simulation_output['potential']['potential_vertices'],
        total_width = simulation_output['geometry']['total_width'],
        total_height = simulation_output['geometry']['total_height'],
        label = r'$\phi~[{\rm V}]$',
        filename = simulation_input['output_folder'] + '/potential.pdf')
    #####################E_x plot##############################################
    plot_function(
        coordinates = simulation_output['geometry']['coordinates'],
        cells = simulation_output['geometry']['cells'],
        function = simulation_output['fields']['E_cells'][:,0],
        total_width = simulation_output['geometry']['total_width'],
        total_height = simulation_output['geometry']['total_height'],
        label = r'$E_x~[{\rm V/\mu m}]$',
        filename = simulation_input['output_folder'] + '/E_x.pdf')
    #####################D_y plot##############################################
    plot_function(
        coordinates = simulation_output['geometry']['coordinates'],
        cells = simulation_output['geometry']['cells'],
        function = (simulation_output['fields']['D_cells'][:,1] / EPSILON_0),
        total_width = simulation_output['geometry']['total_width'],
        total_height = simulation_output['geometry']['total_height'],
        label = r'$D_y/\epsilon_0~[{\rm V/\mu m}]$',
        filename = simulation_input['output_folder'] + '/D_y.pdf')
    #####################epsilon_xx plot#######################################
    plot_function(
        coordinates = simulation_output['geometry']['coordinates'],
        cells = simulation_output['geometry']['cells'],
        function = simulation_output['geometry']['epsilon_cells'][:,0,0],
        total_width = simulation_output['geometry']['total_width'],
        total_height = simulation_output['geometry']['total_height'],
        label = r'$\epsilon_{xx}$',
        filename = simulation_input['output_folder'] + '/epsilon_xx.pdf')
    #########################density plot######################################
    plt.figure(figsize = fs.figsize1d())
    plt.clf()
    ax_dim =fs.axes1d()
    ax_dim[2] *=0.9
    ax_dim[3] *=0.9
    ax = plt.axes(ax_dim)
    plt.plot(simulation_output['sheet_values']['x_sampling'],
             simulation_output['sheet_values']['charge_densities_sheets'].T 
                * 1e8 / (- ELEMENTARY_CHARGE))
    plt.xlim(0, simulation_output['geometry']['total_width'])
    plt.xlabel(r'$x~[{\rm \mu m}]$')
    plt.ylabel(r'$n_e~[{\rm cm^{-2}}]$')
    plt.savefig(simulation_input['output_folder'] + '/n_e.pdf')
    #########################potential on sheets  plot#########################
    plt.figure(figsize = fs.figsize1d())
    plt.clf()
    ax_dim =fs.axes1d()
    ax_dim[2] *=0.9
    ax_dim[3] *=0.9
    ax = plt.axes(ax_dim)
    plt.plot(simulation_output['sheet_values']['x_sampling'],
             simulation_output['sheet_values']['potential_sheets'].T )
    plt.xlim(0, simulation_output['geometry']['total_width'])
    plt.xlabel(r'$x~[{\rm \mu m}]$')
    plt.ylabel(r'$\phi~[{\rm V}]$')
    plt.savefig(simulation_input['output_folder'] + '/potential_sheets.pdf')

def _test_nonlinear():
    simulation_input = {
        'total_width' : .080,
        'layers' : [
            {'index' : 1, 'thickness' : 0.2850, 'material' : 'SiO2'},
            {'index' : 2, 'thickness' : 0.0130, 'material' : 'vacuum'},
            {'index' : 3, 'thickness' : 0.0035, 'material' : 'hBN'}, 
            {'index' : 4, 'thickness' : 0.0060, 'material' : 'hBN'}, 
            {'index' : 5, 'thickness' : 0.1000, 'material' : 'vacuum'}
            ],
        'holes' :  [
            {'index' : 6, 'layer' : 2, 'x1' : .020, 'x2' : .060, 
            'material' : 'conductor'}
            ], 
        'sheets' : [
            {'index' : 7, 'interface' : (np.nan, 1), 'material' : 'conductor'},
            {'index' : 8, 'interface' : (3,4), 'material' :'graphene_SL', 
            'parameters' : {}}
            ], 
        'gate_voltages' : {6 : 1., 7 : 0., 8 : 0.},  
        'output_folder' : 'testing_nonlinear',
        'resolution' : 10,
        'refinement_region' : []
        }
    simulation_output = simulation(**simulation_input)
    #####################domain plot###########################################
    plot_function(
        coordinates = simulation_output['geometry']['coordinates'],
        cells = simulation_output['geometry']['cells'],
        function = simulation_output['geometry']['subdomain_marker'].array(),
        total_width = simulation_output['geometry']['total_width'],
        total_height = simulation_output['geometry']['total_height'],
        label = r'Domain index',
        mesh = simulation_output['geometry']['mesh'],
        facet_function = simulation_output['geometry']['internal_boundary_marker'],
        filename = simulation_input['output_folder'] + '/domains.pdf')
    #####################potential plot########################################
    plot_function(
        coordinates = simulation_output['geometry']['coordinates'],
        cells = simulation_output['geometry']['cells'],
        function = simulation_output['potential']['potential_vertices'],
        total_width = simulation_output['geometry']['total_width'],
        total_height = simulation_output['geometry']['total_height'],
        label = r'$\phi~[{\rm V}]$',
        filename = simulation_input['output_folder'] + '/potential.pdf')
    #####################E_x plot##############################################
    plot_function(
        coordinates = simulation_output['geometry']['coordinates'],
        cells = simulation_output['geometry']['cells'],
        function = simulation_output['fields']['E_cells'][:,0],
        total_width = simulation_output['geometry']['total_width'],
        total_height = simulation_output['geometry']['total_height'],
        label = r'$E_x~[{\rm V/\mu m}]$',
        filename = simulation_input['output_folder'] + '/E_x.pdf')
    #####################D_y plot##############################################
    plot_function(
        coordinates = simulation_output['geometry']['coordinates'],
        cells = simulation_output['geometry']['cells'],
        function = (simulation_output['fields']['D_cells'][:,1] / EPSILON_0),
        total_width = simulation_output['geometry']['total_width'],
        total_height = simulation_output['geometry']['total_height'],
        label = r'$D_y/\epsilon_0~[{\rm V/\mu m}]$',
        filename = simulation_input['output_folder'] + '/D_y.pdf')
    #####################epsilon_xx plot#######################################
    plot_function(
        coordinates = simulation_output['geometry']['coordinates'],
        cells = simulation_output['geometry']['cells'],
        function = simulation_output['geometry']['epsilon_cells'][:,0,0],
        total_width = simulation_output['geometry']['total_width'],
        total_height = simulation_output['geometry']['total_height'],
        label = r'$\epsilon_{xx}$',
        filename = simulation_input['output_folder'] + '/epsilon_xx.pdf')
    #########################density plot######################################
    plt.figure(figsize = fs.figsize1d())
    plt.clf()
    ax_dim =fs.axes1d()
    ax_dim[2] *=0.9
    ax_dim[3] *=0.9
    ax = plt.axes(ax_dim)
    plt.plot(simulation_output['sheet_values']['x_sampling'],
             simulation_output['sheet_values']['charge_densities_sheets'].T 
                * 1e8 / (- ELEMENTARY_CHARGE))
    plt.xlim(0, simulation_output['geometry']['total_width'])
    plt.xlabel(r'$x~[{\rm \mu m}]$')
    plt.ylabel(r'$n_e~[{\rm cm^{-2}}]$')
    plt.savefig(simulation_input['output_folder'] + '/n_e.pdf')
    #########################potential on sheets  plot#########################
    plt.figure(figsize = fs.figsize1d())
    plt.clf()
    ax_dim =fs.axes1d()
    ax_dim[2] *=0.9
    ax_dim[3] *=0.9
    ax = plt.axes(ax_dim)
    plt.plot(simulation_output['sheet_values']['x_sampling'],
             simulation_output['sheet_values']['potential_sheets'].T )
    plt.xlim(0, simulation_output['geometry']['total_width'])
    plt.xlabel(r'$x~[{\rm \mu m}]$')
    plt.ylabel(r'$\phi~[{\rm V}]$')
    plt.savefig(simulation_input['output_folder'] + '/potential_sheets.pdf')
########### Run tests #########################################################
if __name__ == '__main__':
    _test_linear()
    _test_nonlinear()