#####################################
#classes and functions declarations
from dolfin import *
import mshr
###############################################################################
class Matrix_Expression(UserExpression):
    '''class for matrix expression, default dimension is 2X2. 
    Can be initializex with either mesh functions or numbers
    Parameters for initialization:
    chack the markerfunction initialization
    '''
    def __init__(self,
                 entries,
                 marker = None,
                 dimension = 2,
                 scalar = False,
                 *args,
                 **kwargs):
        self.entries = entries #entries of the matrix as a flattened list (C-ordering)
        self.marker = marker
        self.dimension = int(dimension) #dimension of the matrix, default is 2X2
        self.scalar = scalar #whether the matrix is a scalar matrix. Default False.
        #check the type of initialization (numbers or meshfunctions)        
        self.is_constant = all(isinstance(element,(int,float,complex)) 
                               for element in self.entries) and marker == None#true if all the entries are numbers
        self.is_meshfunction = all(isinstance(element,(cpp.mesh.MeshFunctionDouble,cpp.mesh.MeshFunctionInt,cpp.mesh.MeshFunctionSizet)) for element in self.entries) #true if all the entries are meshfunctions
        self.is_markerfunction = not (marker==None) #add checks

        
        #if not (self.is_constant or self.is_meshfunction or self.is_markerfunction): #raise error if initialiozation is not correct
        #   raise TypeError('Expected either all numbers or all MeshFunction for matrix entries')
        #check dimensions
        #if (not scalar) and (len(entries)!=dimension**2):
        #   raise ValueError('Entries number not matching matrix dimension. Expecting %g entries for matrix of dimension %g.'% ((dimension**2),dimension))
        #elif scalar and (len(entries)!=1):
        #   raise ValueError('Entries number not matching matrix dimension. Expecting 1 entry for scalar matrix of dimension %g.'% (dimension))
        UserExpression.__init__(self, *args, **kwargs) #execute the __init__ of the superclass
    def eval_cell(self, value, x, cell):
        if self.is_constant:
            if self.scalar:
                value[:] = self.entries * np.ravel(np.identity(self.dimension))
            else:
                value[:] = self.entries
        elif self.is_meshfunction:
            if self.scalar:
                value[:] = (self.entries[0][cell.index] 
                            * np.ravel(np.identity(self.dimension)))
            else:
                value[:] = [function[cell.index] for function in self.entries]
        elif self.is_markerfunction:
            if self.scalar:
                value[:] = (self.entries[self.marker[cell.index]] 
                            * np.ravel(np.identity(self.dimension)))
            else:
                #print(cell.index)
                #print(self.marker[cell.index])
                #print(self.entries[self.marker[cell.index]])
                value[:] = self.entries[self.marker[cell.index]]
        else:
            raise TypeError('''Expected either all numbers or all 
                             MeshFunction for matrix entries''')
    def value_shape(self):
        if self.dimension >=2:
                return (self.dimension,self.dimension)
        else:
            return ()
################################################################################
def close_to_segment(x, r0, r1, tol):
    '''Function that returns whether a point x is closer than tol to a segment identified by its endpoints r0 and r1.
    Parameters : 
    x :
    r0 :
    r1 :
    tol :
    Returns :'''
    return (abs((r1[1]-r0[1])*(x[0]-r0[0])-(r1[0]-r0[0])*(x[1]-r0[1]))<=
    tol*np.sqrt(((r0[0]-r1[0])**2+(r0[1]-r1[1])**2)) 
    and abs((r1[0]-r0[0])*(x[0]-(r0[0]+r1[0])/2.)+(r1[1]-r0[1])*(x[1]-(r0[1]+r1[1])/2.))
    <=((r0[0]-r1[0])**2+(r0[1]-r1[1])**2)/2.+tol*np.sqrt(((r0[0]-r1[0])**2+(r0[1]-r1[1])**2)))  
################################################################################
def close_to_line(x,line,tol):
    '''returns whether a point is closer than tol to a line. line is a list of points in 2D space like [[0.,0.],[0.,1.],[1.,1.]]'''
    result=False
    for i in range(len(line)-1):
        result=result or close_to_segment(x,line[i],line[i+1],tol)
    return result
###############################################################################
class Boundary_Close_To_Line(SubDomain):
    '''class for boundary points close to a line'''
    def __init__(self, line,tol=DOLFIN_EPS):
        SubDomain.__init__(self)
        self.line=line
        self.tol=tol
    def inside(self,x,on_boundary):
        return on_boundary and close_to_line(x,self.line,self.tol)
class Close_To_Line(SubDomain):
    '''class for boundary points close to a line'''
    def __init__(self, line,tol=DOLFIN_EPS):
        SubDomain.__init__(self)
        self.line=line
        self.tol=tol
    def inside(self,x,on_boundary):
        return close_to_line(x,self.line,self.tol)
class Periodic_Boundary(SubDomain):
    def __init__(self,direction=0,xmin=0,xmax=1):
        self.direction=direction
        self.xmin=xmin
        self.xmax=xmax
        SubDomain.__init__(self)
    def inside(self, x, on_boundary):
        return bool(x[self.direction] < self.xmin+DOLFIN_EPS and x[self.direction] > self.xmin-DOLFIN_EPS and on_boundary)
    def map(self, x, y):
        for i in range(len(x)):
            if i==self.direction:
                y[i]=x[i]-self.xmax+self.xmin
            else:
                y[i]=x[i]


def create_cellfunction(sample_mesh,f):
    '''creates a cell function from a mesh and a python function'''
    fun=MeshFunction("double", sample_mesh, 2)
    for cell in cells(sample_mesh):
        fun[cell] = f(cell.midpoint().x(),cell.midpoint().y())
    return fun
###############################################################################

