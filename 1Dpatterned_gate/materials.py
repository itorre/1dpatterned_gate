#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'Material properties for electrostatic solver'
import numpy as np
import dolfin as dol
##################################### Costants ################################
EPSILON_0 =  8.854187812813e-18 #vacuum dielectric permittivity in F/um
ELEMENTARY_CHARGE = 1.602176634e-19 #elementary charge in C
PLANCK_REDUCED_CONSTANT = 1.054571817e-34 #in J/s
#####################################Dictionary of static dielectric tensors###
dielectric_constants = {
    'vacuum' : np.array([[1.,0.],[0.,1.]]),
    'hBN' : np.array([[6.57,0.],[0.,3.56]]),
    'SiO2' : np.array([[2.51,0.],[0.,2.51]])
    }
###############################################################################
class rho_graphene(dol.UserExpression):
    def __init__(self, phi, V, **kwargs):
        self.phi = phi
        self.V = V
        self.VD = 1e12 #um/s
        self.parameters = kwargs
        dol.UserExpression.__init__(self)
    def eval(self, value, x):
        mu = (self.phi(x) - self.V) * ELEMENTARY_CHARGE
        value[0] = (- mu**2 * np.sign(mu) 
            * ELEMENTARY_CHARGE/(np.pi * PLANCK_REDUCED_CONSTANT**2 * self.VD**2))
    def value_shape(self):
        return ()
class rho_graphene_der(dol.UserExpression):
    def __init__(self, phi, V, **kwargs):
        self.phi = phi
        self.V = V
        self.VD = 1e12 #um/s
        self.parameters = kwargs
        dol.UserExpression.__init__(self)
    def eval(self, value, x):
        mu = (self.phi(x) - self.V) * ELEMENTARY_CHARGE
        value[0] = (- 2. * abs(mu) * ELEMENTARY_CHARGE
            * ELEMENTARY_CHARGE/(np.pi * PLANCK_REDUCED_CONSTANT**2 * self.VD**2))
    def value_shape(self):
        return ()
###############################################################################
rho_dict = {'graphene_SL' : {'funct' : rho_graphene, 
                             'der1' : rho_graphene_der}
           }