Electrostatic simulator for 1D-periodic structures.
Requires:
python3
numpy
matplotlib
dolfin version 2019.1.0
check dolfin version using 
dolfin-version
Installation (Ubuntu)
sudo apt-get install python3, python3-numpy, python3-matplotlib
sudo apt-get install texlive
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:fenics-packages/fenics
sudo apt-get update
sudo apt-get install --no-install-recommends fenics
Installation (Windows)
activate WSL and Ubuntu, use Ubuntu installation